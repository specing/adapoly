with Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

package body Matrix is
   use all type Ada.Containers.Count_Type;


   function Element(m : Matrix; row : Positive; col : Positive) return Number is
     (m.data( (row-1) * m.n_cols + col));

   function Image(m : Matrix) return String is
      use Ada.Strings.Unbounded;
      s : Unbounded_String := Null_Unbounded_String;
   begin
      Append(s, "[" & ASCII.LF);
      for row_index in 1 .. m.n_rows loop
         for col_index in 1 .. m.n_cols loop
            Append(s, m.Element(row_index, col_index)'Image);
            Append(s, ", ");
         end loop;
         Append(s, ASCII.LF);
      end loop;
      Append(s, "]");
      return To_String(s);
   end;


   function IsTransposed(m : Matrix) return Boolean is
   begin
      return m.is_transposed;
   end;


   function n_cols(m : Matrix) return Positive is (m.number_of_cols);
   function n_rows(m : Matrix) return Positive is (m.number_of_rows);




   -- "Eliminate" a row (actually zero the element (elim_row, from_column),
   -- not sure how to call it)
   procedure Eliminate_Row(m : in out Matrix; elim_row,row,from_column : Positive) is
      pivot : Number;
   begin
      -- maybe find first nonzero column. ? Right now we rely on caller to do that (from_column)
      -- Find out how to set (row, from_column) to 1
      pivot := Inverse(m.Element(row, from_column));
      -- and then multiply 1 with the element we are subtracting from
      pivot := pivot * m.Element(elim_row, from_column);

      for c in from_column .. m.n_cols loop
         m.Replace_Element(elim_row, c, m.Element(elim_row, c) - pivot * m.Element(row, c));
      end loop;
   end;


   procedure Replace_Element(m : in out Matrix; row : Positive; col : Positive; n : Number) is
   begin
      m.data( (row-1) * m.n_cols + col) := n;
   end;


   procedure Swap_Rows(m : in out Matrix; r1, r2 : Positive) is
      temp : Number;
   begin
      for col in 1 .. m.n_cols loop
         temp := m.Element(r1, col);
         m.Replace_Element(r1, col, m.Element(r2, col));
         m.Replace_Element(r2, col, temp);
      end loop;
   end;


   procedure Transpose(m : in out Matrix) is
   begin
      m.is_transposed := true;
   end;


   procedure Init(m : in out Matrix; rows : Positive; cols : Positive) is
      num_elements : Ada.Containers.Count_Type :=
        Ada.Containers.Count_Type(rows * cols);
   begin
      if m.data.Length /= 0 then
         raise Program_Error;-- "Already initialised!");
      end if;

      m.number_of_rows := rows;
      m.number_of_cols := cols;
      m.data.Reserve_Capacity(num_elements);
      for i in 1 .. num_elements loop
         m.data.Append(0);
      end loop;
   end;





   function "-"(lhs, rhs : Matrix) return Matrix is
      res : Matrix;
   begin
      if lhs.n_rows /= rhs.n_rows or lhs.n_cols /= rhs.n_cols then
         raise Program_Error;
      end if;

      res.Init(lhs.n_rows, lhs.n_cols);
      for row in 1 .. res.n_rows loop
         for col in 1 .. res.n_cols loop
            res.Replace_Element(row, col, lhs.Element(row, col) - rhs.Element(row, col));
         end loop;
      end loop;
      return res;
   end;


   function Identity(n : Positive) return Matrix is
      m : Matrix;
   begin
      m.Init(n, n);
      for i in 1 .. n loop
         m.Replace_Element(i, i, 1);
      end loop;
      return m;
   end;


   function kernel_basis(m : Matrix) return Matrix is
      re : Matrix := m.row_eliminated;

      n_basis_vectors : Natural := 0;

      temp : Matrix;
   begin
      -- The former row-reduced form will have a '1' in the column
      -- corresponding to the variable that can be obtained from others.
      -- such a column is guaranteed to contain zeros above and below the '1'.
      temp.Init(rows => re.n_cols, cols => re.n_rows);
      declare
         row : Positive := 1;
         col : Positive := 1;
      begin
         -- col stands for x[col] in Mx = 0
         while row <= re.n_rows and col <= re.n_cols loop
            if re.Element(row, col) = 0 then
               -- Rows that have no leading '1' correspond to free variables
               temp.Replace_Element(col, col, 1);
               col := col + 1; -- find first nonzero
               n_basis_vectors := n_basis_vectors + 1;
            else
               -- The variable corresponding to this column can be written as..
               for c in col + 1 .. re.n_cols loop
                  temp.Replace_Element(col, c, 0 - re.Element(row, c));
               end loop;
               row := row + 1;
               col := col + 1;
            end if;
         end loop;
      end;

      -- Now, copy all-zero columns to the matrix of basis vectors
      declare
         basis : Matrix;
         dest_col : Positive := 1;
         column_is_nonzero : Boolean;
      begin
         basis.Init(temp.n_rows, n_basis_vectors);

         for col in  1 .. temp.n_cols loop
            column_is_nonzero := False;

            for row in 1 .. temp.n_rows loop
               if temp.Element(row, col) /= 0 then
                  basis.Replace_Element(row, dest_col, temp.Element(row, col));
                  column_is_nonzero := True;
               end if;
            end loop;
            -- Advance to filling the next column
            if column_is_nonzero then
               dest_col := dest_col + 1;
            end if;
         end loop;

         return basis;
      end;
   end;


   function row_eliminated(m : Matrix) return Matrix is
      res : Matrix := m;

      pivot_row : Positive := 1;
      temp : Number;
   begin
      -- reduce downwards
      for col in 1 .. res.n_cols loop
         -- find first nonzero row and swap it, so it becomes the active row.
         for row in pivot_row .. res.n_rows loop
            if res.Element(row, col) /= 0 then
               if row /= pivot_row then -- don't swap with itself.
                  res.Swap_Rows(pivot_row, row);
               end if;
               exit;
            end if;
         end loop;
         -- If we do actually have a non-zero element in this column, then use
         -- it to eliminate nonzero elements in all rows beneath.
         if res.Element(pivot_row, col) /= 0 then
            -- First, ensure that the leftmost element is one. This will also
            -- prove useful later, when we look for basis vectors of the kernel.
            if res.Element(pivot_row, col) /= 1 then
               temp := Inverse(res.Element(pivot_row, col));
               for c in col .. res.n_cols loop
                  res.Replace_Element(pivot_row, c, temp * res.Element(pivot_row, c));
               end loop;
            end if;

            -- actual processing of the rows below
            for row in pivot_row + 1 .. res.n_rows loop
               if res.Element(row, col) /= 0 then
                  res.Eliminate_Row(elim_row => row, row => pivot_row, from_column => col);
               end if;
            end loop;
            -- Active row now moves down.
            pivot_row := pivot_row + 1;
         end if;
      end loop;

      -- Eliminate in the upwards direction, making sure that the leading
      -- nonzero elements of rows have zeros above them.
      for row in reverse 1 .. res.n_rows loop
         -- find first nonzero column
         for col in 1 .. res.n_cols loop
            if res.Element(row, col) /= 0 then
               --Put_Line("Nonzero row: " & Positive'Image(nonzero_row) & " col: " & Positive'Image(col));
               -- Eliminate all nonzero elements in rows above this column
               for r in 1 .. row - 1 loop
                  res.Eliminate_Row(elim_row => r, row => row, from_column => col);
               end loop;

               exit;
            end if;
         end loop;
      end loop;

      return res;
   end;

end Matrix;
