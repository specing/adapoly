with Ada.Containers.Vectors;
with Modular_Arithmetic; use Modular_Arithmetic;

package Matrix is
   -- Indexing is from 1 to n in both dimensions

   type Matrix is tagged private;


   -- Basic operations, might be moved "upwards" to further abstract them.
   function Element(m : Matrix; row : Positive; col : Positive) return Number;

   function Image(m : Matrix) return String;

   function IsTransposed(m : Matrix) return Boolean
     with Inline;

   function n_cols(m : Matrix) return Positive;
   function n_rows(m : Matrix) return Positive;



   -- Gaussian elimination: eliminate elim_row using row, from_column onwards
   -- (assume zeroes on all preceding columns)
   procedure Eliminate_Row(m : in out Matrix; elim_row,row,from_column : Positive);
   -- Todo: references
   procedure Replace_Element(m : in out Matrix; row : Positive; col : Positive; n : Number);

   procedure Swap_Rows(m : in out Matrix; r1, r2 : Positive);

   procedure Transpose(m : in out Matrix);



   -- TODO: controlled type with init
   procedure Init(m : in out Matrix; rows : Positive; cols : Positive);



   function "-"(lhs, rhs : Matrix) return Matrix;

   function Identity(n : Positive) return Matrix;

   -- A matrix of basis vectors, stored in columns.
   function kernel_basis(m : Matrix) return Matrix;

   -- Gaussian elimination
   function row_eliminated(m : Matrix) return Matrix;

private
   package vectors is new Ada.Containers.Vectors(Index_Type => Positive,
                                                 Element_Type => Number);

   type Matrix is tagged
      record
         is_transposed : Boolean;

         number_of_rows : Positive;
         number_of_cols : Positive;
         -- The matrix is stored one row after another in vector form.
         data : vectors.Vector;
      end record;

end Matrix;
