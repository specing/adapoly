with Ada.Text_IO; use Ada.Text_IO;
with Matrix;
with Modular_Arithmetic; use Modular_Arithmetic;
with Vector;
with Polynomial;
with Ada.Exceptions; use Ada.Exceptions;
with Ada.Containers.Doubly_Linked_Lists;


procedure Main is

   function CalculateQ(mod_poly : Polynomial.Polynomial) return Matrix.Matrix is
      use Polynomial;

      Q : Matrix.Matrix;

      h : Polynomial.Polynomial;
      x_to_p : Polynomial.Polynomial;
   begin
      -- x_to_p(x) = x^p
      x_to_p.Replace_Element(Number'Modulus, 1);
      -- Initial h(x) = 1
      h.Replace_Element(index => 0, n => 1);
      Q.Init(rows => mod_poly.Highest_Index, cols => mod_poly.Highest_Index);

      for col in 1 .. Q.n_cols loop
         -- Fill in h
         for row in 1 .. Q.n_rows loop
            Q.Replace_Element(row => row, col => col, n => h.Element(row - 1));
         end loop;

         h := Polynomial.Remainder(x_to_p * h, mod_poly);
      end loop;

      return Q;
   end;


   package Polynomial_List is new Ada.Containers.Doubly_Linked_Lists(Element_Type => Polynomial.Polynomial,
                                                                     "=" => Polynomial."=");
   use all type Polynomial_List.Cursor;

   poly : Polynomial.Polynomial := Polynomial.From_Array((2, 2, 0, 1, 1, 1)); -- p(x) = x^5 + x^4 + x^3 + 2x + 2
   p2 : Polynomial.Polynomial := Polynomial.From_Array((1, 1)); -- 1 + x
   Q, Q_basis : Matrix.Matrix;
   use all type Matrix.Matrix;
   v : Polynomial.Polynomial;

   factors, new_factors : Polynomial_List.List := Polynomial_List.Empty_List;
begin
   Put_Line(Polynomial.gcd (poly, p2).Image);
   -- Oris programa:
   -- 1. Predstavitev matrik, vektorjev in stevil
   Put_Line("Polynomial: " & poly.Image);
   -- 2. racunanje Q
   Q := CalculateQ(poly);
   -- 2.1 mnozenje polinomov po modulu polinoma
   Q := Q - Matrix.Identity(Q.n_rows);
   -- 3. Resevanje zgornjega sistema (iskanje baze Ker(Q-I))
   Put_Line("Q - I = " & Image(Q));

   Q := Q.row_eliminated;
   Put_Line("Q - I reduced = " & Image(Q));

   Q_basis := Q.kernel_basis;
   Put_Line("Basis vecs: " & Image(Q_basis));
   -- 4. Testiranje resitev (polinomski gcd)


   -- The factors set first contains only the given polynomial itself
   factors.Append(poly);

   for basis_i in 2 .. Q_basis.n_cols loop
      exit when Natural(factors.Length) = Q_basis.n_cols;
      v := Polynomial.From_Column(Q_basis, basis_i);

      Put_Line("Testing basis v = " & Polynomial.Image(v));

      for a in 0 .. Number'Modulus - 1 loop
         exit when Natural(factors.Length) = Q_basis.n_cols;

         v.Replace_Element(0, Number(a));

         -- Try to further factorise all current factors
         for cursor in factors.iterate loop
            p2 := Polynomial.gcd(factors.Reference(cursor), v);
            Put_line("gcd(" & factors.Reference(Cursor).Image & ", " & v.Image & " - " & Integer'Image(a) & " = " & p2.Image);

            if p2.Highest_Index /= 0 then
               Put_Line("Found divisor: " & Polynomial.Image(p2));

               new_factors.Append(p2);
               new_factors.Append(Polynomial."/"(factors.Reference(cursor), p2));
            else
               new_factors.Append(factors.Reference(cursor)); -- carry it over
            end if;
         end loop;

         factors := new_factors;
      end loop;
   end loop;

   for cursor in factors.Iterate loop
      Put_Line("Factor: " & factors.Reference(cursor).Image);
   end loop;

exception
   when Event: others =>
      Put_Line(Exception_Message(Event));--
      Put_Line(Exception_Information(Event));
end Main;
