package body Modular_Arithmetic is

   function Inverse(n : Number) return Number is
      r : Number := 1;
   begin
      -- rich man's inverse computation, based on Fermat's
      -- little theorem (a^p \equiv a mod p)
      for i in 1 .. number'Modulus - 2 loop
         r := r * n;
      end loop;
      return r;
   end;

end Modular_Arithmetic;
