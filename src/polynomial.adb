with Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
with Matrix; use Matrix;
with Modular_Arithmetic; use Modular_Arithmetic;
use all type Modular_Arithmetic.Number;

package body Polynomial is
   use type Ada.Containers.Count_Type;
   use type vectors.Vector;


   function "="(lhs, rhs : Polynomial) return Boolean is
     (lhs.elements = rhs.elements);

   function Element(p : Polynomial; index : Natural) return Number is
   begin
      if index > p.Highest_Index then
         return 0;
      else
         return p.elements.Element(index);
      end if;
   end;


   function Highest_Index(p : Polynomial) return Natural is
   begin
      return Natural(p.elements.Length) - 1;
   end;


   function Image(p : Polynomial) return String is
      use Ada.Strings.Unbounded;
      s : Unbounded_String := Null_Unbounded_String;
   begin
      for i in 0 .. p.Highest_Index loop
         if p.Highest_Index = 0 or i = p.Highest_Index or p.Element(i) /= 0 then
            Append(s, Number'Image(p.Element(i)));
            if i /= 0 then
               Append(s, "x^");
               Append(s, Integer'Image(i));
            end if;
            if i /= p.Highest_Index then
               Append(s, " + ");
            end if;
         end if;
      end loop;

      return To_String(s);
   end;


   function leading_coefficient(p : Polynomial) return Number is
      (p.elements.Last_Element);





   procedure Replace_Element(p : in out Polynomial; index : Natural; n : Number) is
   begin
      if n /= 0 then
         while index > p.Highest_Index loop
            p.elements.Append(0);
         end loop;

         p.elements.Replace_Element(index, n);
      else
         if index > p.Highest_Index then
            null;
         elsif index /= 0 and index = p.Highest_Index then
            p.elements.Delete_Last;
            -- adjust length
            while p.Highest_Index > 0 and p.Leading_Coefficient = 0 loop
               p.elements.Delete_Last;
            end loop;
         else
            p.elements.Replace_Element(index, n);
         end if;
      end if;
   end;





   function From_Array(d : number_array) return Polynomial is
      p : Polynomial;
   begin
      p.elements.Reserve_Capacity(Ada.Containers.Count_Type(d'Length));
      for i in d'Range loop
         p.Replace_Element(index => i - d'First, n => d(i));
      end loop;
      return p;
   end;


   function From_Column(M : Matrix.Matrix; col : Positive) return Polynomial is
      p : Polynomial;
   begin
      for row in reverse 1 .. M.n_rows loop
         p.Replace_Element(index => row - 1, n => M.Element(row, col));
      end loop;

      return p;
   end;





   function "*"(lhs, rhs : Polynomial) return Polynomial is
      p : Polynomial;
   begin
      for lhs_index in 0 .. lhs.Highest_Index loop
         for rhs_index in 0 .. rhs.Highest_Index loop
            p.Replace_Element(lhs_index + rhs_index,
                              lhs.Element(lhs_index) * rhs.Element(rhs_index));
         end loop;
      end loop;

      return p;
   end;


   -- pseudocode from https://en.wikipedia.org/wiki/Polynomial_greatest_common_divisor
   function GCD(a, b : Polynomial) return Polynomial is
   begin
      if b.Highest_Index = 0 and b.Element(0) = 0 then
         return a;
      else
         return GCD(b, Remainder(a, b));
      end if;
   end;


   function Divide(divided, divisor : Polynomial) return Division_Result is
      dr : Division_Result;

      len_diff : Integer;
      mult : Number;
   begin
      if divisor.Highest_Index = 0 and divisor.Element(0) = 0 then
         raise Program_Error;
      end if;

      dr.remainder := divided;

      while dr.remainder.Highest_Index >= divisor.Highest_Index loop
         if dr.remainder.Highest_Index = 0 and dr.remainder.Element(0) = 0 then
            return dr;
         end if;

         -- Difference between orders of result(divided) and divisor
         len_diff := dr.remainder.Highest_Index - divisor.Highest_Index;
         -- by how much do we have to multiply the leading coefficient of
         -- divisor to destroy the leading coefficient of result?
         mult := Inverse(divisor.leading_coefficient) * dr.remainder.leading_coefficient;

         dr.quotient.Replace_Element(len_diff, mult);
         -- Multiply divisor by cx^k such that its leading coefficient
         -- equals the leading coeficient of result(divided), then subtract
         -- such a polynomial from divided. Repeat until divided has a higher
         -- or equal order to result.
         --Put_Line("Divided: " & dr.remainder.Image);
         --Put_Line("Divisor: " & divisor.Image);
         --Put_Line("cx^k = " & Number'Image(mult) & "x^" & Integer'Image(len_diff));

         for i in 0 .. divisor.Highest_Index loop
            dr.remainder.Replace_Element(i + len_diff,
                                         dr.remainder.Element(i + len_diff) - mult*divisor.Element(i));
         end loop;

         --Put_Line("remainder: " & dr.remainder.Image);
         --Put_Line("quotient:  " & dr.quotient.Image);
         --Put_Line("");
      end loop;

      return dr;
   end;






   procedure Initialize(p : in out Polynomial) is
   begin
      p.elements.Append(0);
   end;





   -- TODO
   function Length(p : Polynomial) return Natural is
     (Natural(p.elements.Length));


   function Order(p : Polynomial) return Integer is
   begin
      if p.Length = 1 and p.Element(1) = 0 then
         return -1;
      else
         return p.Length - 1;
      end if;
   end;

end Polynomial;
