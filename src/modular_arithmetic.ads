package Modular_Arithmetic is

   type Number is mod 3;
   type number_array is array (Integer range <>) of Number;

   -- Helpers
   function Inverse(n : Number) return Number;

end Modular_Arithmetic;
