package body vector is

   function Is_Row_Vector(v : Vector) return Boolean is
     (v.is_row);


   function Is_Col_Vector(v : Vector) return Boolean is
     (not Is_Row_Vector(v));


   function Length(v : Vector) return Positive is
     (Positive(v.elements.Length));



   function Element(v : Vector; index : Positive) return Number is
     (v.elements.Element(index));

   -- Todo: references
   procedure Replace_Element(v : in out Vector; index : Positive; n : Number) is
   begin
      v.elements.Replace_Element(index, n);
   end;

   -- TODO: controlled type with init
   procedure Init(v : in out Vector; len : Positive; is_row_vector : Boolean := True) is
   begin
      v.elements.Reserve_Capacity(Ada.Containers.Count_Type(len));
      for i in 1 .. len loop
         v.elements.Append(0);
      end loop;

      v.is_row := is_row_vector;
   end;

   procedure Init(v : in out Vector; d : vector_array; is_row_vector : Boolean := True) is
   begin
      v.elements.Reserve_Capacity(Ada.Containers.Count_Type(d'Length));
      for i in d'First .. d'Last loop
         v.elements.Append(d(i));
      end loop;
   end;

end vector;
