with Ada.Containers.Vectors;
with Ada.Finalization;

with Modular_Arithmetic; use Modular_Arithmetic;
with Matrix;


-- A poly of order n is stored as a vector with n+1 elements
package Polynomial is

   type Polynomial is tagged private;

   type Division_Result is
      record
         remainder : Polynomial;
         quotient  : Polynomial;
      end record;


   function "="(lhs, rhs : Polynomial) return Boolean;

   -- Returns 0 if element is 0 or index does not exist
   function Element(p : Polynomial; index : Natural) return Number;

   -- the highest order, or 0 if it would be negative.
   --function Length(p : Polynomial) return Natural;
   -- index of highest order element
   function Highest_Index(p : Polynomial) return Natural;

   function Image(p : Polynomial) return String;

   function Leading_Coefficient(p : Polynomial) return Number;



   -- Grows / shrinks size of p as needed, TODO: references?
   procedure Replace_Element(p : in out Polynomial; index : Natural; n : Number);





   function From_Array(d : number_array) return Polynomial;

   function From_Column(M : Matrix.Matrix; col : Positive) return Polynomial;


   function "*"(lhs, rhs : Polynomial) return Polynomial;

   function Divide(divided, divisor : Polynomial) return Division_Result;

   function "/"(divided, divisor : Polynomial) return Polynomial is
      (Divide(divided, divisor).quotient);
   function Remainder(divided, divisor : Polynomial) return Polynomial is
      (Divide(divided, divisor).remainder);

   function GCD(a, b : Polynomial) return Polynomial;

private

   package vectors is new Ada.Containers.Vectors(Index_Type => Natural,
                                                 Element_Type => Modular_Arithmetic.Number);

   type Polynomial is new Ada.Finalization.Controlled with
      record
         elements : vectors.Vector;
      end record;


   procedure Initialize(p : in out Polynomial);

end Polynomial;
