with Ada.Containers.Vectors;
with Modular_Arithmetic; use Modular_Arithmetic;

package vector is

   type vector is tagged private;
   type vector_array is array (Positive range <>) of Number;


   function Is_Row_Vector(v : Vector) return Boolean;
   function Is_Col_Vector(v : Vector) return Boolean;
   function Length(v : Vector) return Positive;


   function Element(v : Vector; index : Positive) return Number;

   -- Todo: references
   procedure Replace_Element(v : in out Vector; index : Positive; n : Number);


   -- TODO: controlled type with init
   procedure Init(v : in out Vector; len : Positive; is_row_vector : Boolean := True);

   procedure Init(v : in out Vector; d : vector_array; is_row_vector : Boolean := True);
private

   package vectors is new Ada.Containers.Vectors(Index_Type => Positive,
                                                 Element_Type => Number);

   type vector is tagged
      record
         is_row : Boolean;

         number_of_elements : Positive;
         elements : vectors.Vector;
      end record;

end vector;
